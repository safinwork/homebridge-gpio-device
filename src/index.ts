import DeviceAccessory from "./utils/device.accessory";

export default (homebridge: any) => {
    console.log("homebridge-gpio-device API version: " + homebridge.version);

    homebridge.registerAccessory("homebridge-gpio-device", "GPIODevice", DeviceAccessory);
}

