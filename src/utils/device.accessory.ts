import {Logging} from "homebridge";
import {AccessoryConfig} from "homebridge/lib/server";
import {API} from "homebridge/lib/api";
import Gpio from "./gpio";
import BaseDevice from "../devices/base.device";

export type Config = AccessoryConfig & { type: string, pin?: number, pins: [number, number] };

export default class DeviceAccessory {
    private services = [];
    private readonly device: BaseDevice;

    constructor(private logger: Logging, private config: Config, private api: API) {
        if (!config.type) throw new Error("'type' parameter is missing");
        if (!config.name) throw new Error("'name' parameter is missing for accessory " + config.type);
        if (!config.hasOwnProperty('pin') && !config.pins) throw new Error("'pin(s)' parameter is missing for accessory " + config.name);

        const infoService = new api.hap.Service.AccessoryInformation();
        infoService.setCharacteristic(api.hap.Characteristic.Manufacturer, 'Raspberry')
        infoService.setCharacteristic(api.hap.Characteristic.Model, config.type)

        this.services.push(infoService);

        switch (config.type) {
            case 'ContactSensor':
            case 'MotionSensor':
            case 'LeakSensor':
            case 'SmokeSensor':
            case 'CarbonDioxideSensor':
            case 'CarbonMonoxideSensor':
                this.device = new DigitalInput(this, logger, config);
                break;
            case 'Switch':
            case 'Lightbulb':
            case 'Outlet':
            case 'Fan':
            case 'Fanv2':
            case 'Faucet':
            case 'IrrigationSystem':
            case 'Valve':
            case 'Speaker':
            case 'Microphone':
                this.device = new DigitalOutput(this, logger, config);
                break;
            case 'Door':
            case 'Window':
            case 'WindowCovering':
                this.device = new RollerShutter(this, logger, config);
                break;
            case 'GarageDoorOpener':
                this.device = new GarageDoor(this, logger, config);
                break;
            case 'LockMechanism':
                this.device = new LockMechanism(this, logger, config);
                break;
            case 'StatelessProgrammableSwitch':
            case 'Doorbell':
                this.device = new ProgrammableSwitch(this, logger, config);
                break;
            case 'Thermostat':
                this.device = new Thermostat(this, logger, config);
                break;
            case 'Wiegand':
                this.device = new Wiegand(this, logger, config);
                break;
            default:
                throw new Error("Unknown 'type' parameter : " + config.type);
        }
        Gpio.devices[this.getDeviceTypeUid(config.type)] = this.device;
    }

    getDeviceTypeUid(type) {
        if (!Gpio.devices[type]) {
            return type;
        }
        let i = 1;
        while (true) {
            if (!Gpio.devices[type + "_" + i]) {
                return type + "_" + i;
            }
            i++;
        }
    }

    // noinspection JSUnusedGlobalSymbols
    getServices(): any {
        return this.services;
    }
}