import {BinaryValue, Gpio as GpioOnOff, High, Low, ValueCallback} from 'onoff';

export const HIGH = GpioOnOff.HIGH;
export const LOW = GpioOnOff.LOW;

export enum Direction {
    in = 'in',
    out = 'out'
}

export enum Edge {
    both = 'both',
    falling = 'falling',
    rising = 'rising',
    none = 'none'
}

export default class Gpio {
    static HIGH: High = GpioOnOff.HIGH;
    static LOW: Low = GpioOnOff.LOW;
    static io: { [key: number]: GpioOnOff; } = {};
    static devices: any = {};

    static init(pin: number, direction: Direction, edge: Edge, callback?: ValueCallback) {
        if (direction == Direction.in) {
            this.io[pin] = new GpioOnOff(pin, direction, edge, {debounceTimeout: 10});
            this.io[pin].watch(callback);
        } else {
            this.io[pin] = new GpioOnOff(pin, direction);
        }
    }

    static read(pin: number): BinaryValue {
        return this.io[pin].readSync();
    }

    static write(pin: number, value: BinaryValue) {
        return this.io[pin].writeSync(value);
    }

    static delay(milliseconds: number) {
        const start = new Date().getTime();
        for (let i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    }
}