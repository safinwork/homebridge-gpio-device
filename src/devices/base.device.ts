import {API} from "homebridge/lib/api";
import {Logging} from "homebridge";
import {Config} from "../utils/device.accessory";

abstract class BaseDevice {
    protected constructor(public api: API, public log: Logging, public config: Config) {
    }
}